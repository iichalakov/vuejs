import mutations from "./mutation-types";

export default {
  [mutations.SET_CARDS](state, cards) {
    state.cards = cards;
  },

  [mutations.CLEAR_CARDS](state) {
    state.cards = [];
  },
  [mutations.SET_CURRENT_CARD](state, currentCard) {
    state.currentCard = currentCard;
  }
};
