import mutations from "./mutation-types";
import actions from "./action-types";
import dataService from "../api/data-service";

export default {
  [actions.GET_CARDS]({ commit }) {
    let cards = [];
    let count = 0;

    dataService
      .getCards()
      .then(responseCards => {
        //console.log(responseCards);
        // console.log(responseCards.data.cards);
        responseCards.data.forEach(element => {
          //console.log(element);
          if (count < 22) {
            let card = {
              cardId: element.cardBackId,
              name: element.name,
              imgAnimated: element.imgAnimated,
              description: element.description,
              sourceDescription: element.sourceDescription
            };
            cards.push(card);
            count++;
            console.log("Count = " + count);
          }
        });
      })
      .then(() => {
        console.log(cards);
        commit(mutations.SET_CARDS, cards);
      });
  }, //end commit

  [actions.CLEAR_CARDS]({ commit }) {
    commit(mutations.CLEAR_CARDS);
  }, //end commit

  [actions.GET_CARDS_DETAILS]({ commit }, cardBackId) {
    dataService.getCardDetails(cardBackId, responseCards => {
      commit(mutations.SET_CURRENT_CARD, responseCards);
    });
  }
};
