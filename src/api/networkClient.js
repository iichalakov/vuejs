import axios from "axios";
import constants from "../constants";

export default {
  get(url, success, failure, params) {
    this.request({
      method: "get",
      url,
      params,
      success,
      failure
    });
  },

  request: (options = {}) => {
    if (!options.url) {
      console.log("URL is required");
      return;
    }

    const data = Object.assign(
      {
        method: "get",
        url: constants.url
      },
      options
    );

    data.params = Object.assign(options.params, { api_key: constants.apiKey });
    axios(data)
      .then(response => {
        if (options.success) {
          options.success(response.data);
        }
      })
      .catch(error => {
        if (options.failure) {
          options.failure(error);
        }
      });
  }
};
