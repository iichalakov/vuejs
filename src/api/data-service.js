import axios from "axios";
import networkClient from "./networkClient";
export default {
  getCards() {
    return axios.get(
      "https://omgvamp-hearthstone-v1.p.rapidapi.com/cardbacks",
      {
        headers: {
          "X-RapidAPI-Key": "nZJ8tkXeR4mshBHQ8ih6oKQ8wF4qp1keYtyjsnMHqZuN4Lj1dU"
          //   Accept: "application/json",
          //   "Content-Type": "application/x-www-form-urlencoded"
        }
      }
    );
  },
  getCardDetails(id, success, failure) {
    networkClient.get(`cardbacks/${id}`, success, failure, id);
  }
};
